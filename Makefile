stackname = nemushee
stackfiles = -c infrastructures/networks.yml \
-c infrastructures/volumes.yml \
-c services/db.yml \
-c services/es.yml \
-c services/redis.yml \
-c services/sidekiq.yml \
-c services/storage.yml \
-c services/streaming.yml \
-c services/web.yml
envfile = env/stack.env

include $(envfile)
export

up:
	docker stack deploy $(stackfiles) $(stackname)

down:
	docker stack rm $(stackname)

restart: down up

config:
	docker stack config $(stackfiles)
